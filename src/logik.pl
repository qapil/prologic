%====================================================================
%====================================================================
% fungujici vstupy : (2,8),(3,4),(4,3),(5,2)
%   nez pretece zasobnik
%====================================================================
%====================================================================


%====================================================================
 % start(+P,+B) 
 % spusti program, P je pocet pozic, B je pocet barev
%====================================================================
start(P,B):-
	logik([],[],P,B).

%====================================================================
 % logik(+Tahy,+Hodnoceni,+#Pozic,+#Barev).
 % na zaklade predchozich hodnoceni a tahu a nove aktualni volby
 % vygeneruje vzor, ohodnoti tah a pokracuje dalsim tahem
 % ukonci se jakmile je v poslednim predanem hodnoceni stejne spravne
 % umistenych barev jako pocet pozic
%====================================================================
logik(_,[[P|_]|_],P,_):-
	write('Uhodl jsi.'),nl.
logik(Tahy,Hodn,P,B):-
	cti(T),
	zvol(T,Tahy,Hodn,P,B,Volba,Pocet),	% Pocet vraci je informativne pro hrace
	hodnot(Volba,T,H),
	pis(H,Pocet),
	logik([T|Tahy],[H|Hodn],P,B).

%====================================================================
 % zvol(+posledni tah,+minule tahy,+minule hodnoceni,
 %	+#pozic,+#barev,-zvoleny vzor,-pocet moznosti)
 % vrati vzor, ke kteremu existuje nejvice moznosti
%====================================================================
zvol(T,Tahy,Hodn,Pozice,Barvy,Volba,Pocet):-
	seznam1(Pozice,1,Sez),
	generuj(Sez,Barvy,T,Tahy,Hodn,Volba,Pocet).

%====================================================================
 % seznam1(+N,+B,-Seznam)
 % seznam B-cek delky N
%====================================================================
seznam1(0,_,[]).
seznam1(N,B,[B|S]):-
	N1 is N-1,
	seznam1(N1,B,S).

%====================================================================
 % seznam2(+As,+B,-Seznam)
 % seznam B-cek stejne delky jako As
%====================================================================
seznam2([],_,[]).
seznam2([A|As],B,[B|S]):-
	seznam2(As,B,S).

%====================================================================
 % generuj(+zkouseny vzor,+# barev,+tah,+minule tahy,+minule hodnoceni,
 %	   -nelepsi vzor,-pocet)
 % vrati vzor (Volba), ke kteremu existuje nejvice moznosti
%====================================================================
generuj(S,B,T,Tahy,Hodn,Volba,Pocet):-
	seznam2(S,B,Posl),
	!,
	(
	S==Posl
	->	P1= -1
	;	(
		pricti(B,S,S1),
		generuj(S1,B,T,Tahy,Hodn,V1,P1)
		)
	),
	(
	testuj(S,Tahy,Hodn)
	->	(
		hodnot(S,T,H),			% S je vzor, T je typ
		seznam2(S,1,Prvni),
		pocitej(B,Prvni,[T|Tahy],[H|Hodn],P)
		)
	;	P= -1
	),
	(
	P1 > P
	->	(
		Volba = V1,
		Pocet = P1
		)
	;	(
		Volba = S,
		Pocet = P
		)
	).

%====================================================================
 % pricti(+N,+I,-O)
 % generuje dalsi seznam prictenin jednoho cisla (dalsi barva)
%====================================================================
pricti(_,[],[]).
pricti(N,[I|Is],[O|Os]):-
	I<N ->	(
		O is I+1,
		Os=Is
		)
	    ;	(
		O is 1,
		pricti(N,Is,Os)
		).
%====================================================================
 % testuj(+Vzor,+minule tahy,+milule hodnoceni)
 % testuje zda Vzor odpovida predchozim tahum a ohodnocenim
%====================================================================
testuj(_,[],[]).
testuj(V,[T|Ts],[H|Hs]):-
	hodnot(V,T,H),
	testuj(V,Ts,Hs).

%====================================================================
 % pocitej(+#barev,+zkousena kombinace,+minule tahy,+minule hodnoceni,-Pocet)
 % vrati pocet smysluplnych tahu podle predchozich hodnoceni a typu
%====================================================================
pocitej(B,S,T,H,Pocet):-
	seznam2(S,B,Posl),
	!,
	(
	S==Posl
	->	P=0
	;	(
		pricti(B,S,S1),
		pocitej(B,S1,T,H,P)
		)
	),
	(
	not(member(S,T)),
	testuj(S,T,H)
	->	Pocet is P+1
	;	Pocet = P
	).

%====================================================================
 % hodnot(+Vzor,+Tip,-[#Spravne,#Vedle]).
 % vrati hodnoceni Tipu podle Vzoru
%====================================================================
hodnot(A,B,[S,V]):-
	spravne(A,B,A1,B1,S),
	!,
	vedle(A1,B1,V).

%====================================================================
 % spravne(+A,+B,-A0,-B0,-Spravne)
 % S je pocet barev umistenych spravne
%====================================================================
spravne([],[],[],[],0).
spravne([X|As],[X|Bs],A1,B1,S):-
	spravne(As,Bs,A1,B1,S1),
	!,
	S is S1+1.
spravne([A|As],[B|Bs],[A|A1],[B|B1],S):-
	spravne(As,Bs,A1,B1,S).

%====================================================================
 % vedle(+A,+B,-Vedle)
 % V je pocet barev umistenych vedle
%====================================================================
vedle(A,B,V):-
	vedle1(A,B,_,V).

vedle1([],B,B,0).
vedle1([A|As],B,Bz,V):-
	vedle1(As,B,B1,V1),
	vypust(A,B1,Bz,V1,V).

%====================================================================
 % vypust(+A,+B,-Bz,+V1,-V)
 % v Bz vrati seznam B bez prvniho vyskytu prvku A, pokud doslo k
 % vypusteni prvku vrati ve V V1+1 jinak  V=V1
%====================================================================
vypust(_,[],[],V,V).
vypust(A,[A|B],B,V1,V):-
	!,			% nesmi zacit backtracking
	V is V1+1.
vypust(A,[X|B],[X|Bz],V1,V):-
	vypust(A,B,Bz,V1,V).

%====================================================================
 % cti(-Tah)
 % precte tah od hrace
%====================================================================
cti(Tah):-
	write('Zadej tip '),
	read(Tah).

%====================================================================
 % pis(+[Spravne,Vedle],+Pocet)
 % vypise hodnoceni tahu a pocet tahu, ktere odpovidaji predchozim
 % typum a hodnocenim (jsou smysluplna)
%====================================================================
pis([S,V],Pocet):-
	write(' spravne: '),write(S),nl,
	write(' vedle  : '),write(V),nl,
	write('mas jeste '),write(Pocet),write(' moznosti jak hrat.'),nl,nl.

%====================================================================

 % a to je konec...
